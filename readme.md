# 小程序双向滑动选择器
![](img.png)
```javascript
        <ani-slider
		width="300"
		slider-value="{{sliderVal}}"
		min="0"
		max="100000"
		step="10"
		disabled="{{disabled}}"
		block-color="yellow"
		active-color="red"
		block-size="26"
		background-color="yellow"
		bindchange="sliderChange"
	>
		<text slot="left" class="slider_left">{{min}}</text>
		<text slot="right" class="slider_right">{{max}}</text>
	</ani-slider>
```
###参数说明
| 属性 | 类型 | 默认值 | 必填 | 说明|
| :-----| ----: | :----: |:----: |:----: |
| width | Number | 320 | 否 | 滑动选择器宽度 |
| sliderValue | Object | {min:0,max:100} | 否 | 焦点值 |
| min | Number | 0 | 否 | 最小范围 |
| max | Number | 100 | 否 | 最大范围 |
| step | Number | 1 | 否 | 步长 |
| disabled | Boolean | false | 否 | 是否禁用 |
| active-color | String | #000000 | 否 | 已选择线的颜色 |
| background-color | String | #D4D4D4 | 否 | 背景线的颜色 |
| block-size| Number | 26 | 否 | 滑块大小 |
| bindchange| Function |  | 否 | 滑动时触发的事件 |
| bindchangeend| Function |  | 否 | 滑动结束时触发的事件 |