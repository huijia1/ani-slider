const app = getApp()

Page({
	data: {
		sliderVal:{
			min:100,
			max:100000
		},
		disabled:false,
		min:100,
		max:100000
	},
	onLoad() {

	},
	onShow(){
		
	},
	sliderChange(e){
		const {min,max,position}=e.detail;
		this.setData({
			min,max
		})
		console.log(min,max,position);
	},
	sliderChangeEnd(){
		console.log('滑动结束');
	},
	changeVal(){
		this.setData({
			sliderVal:{
				min:0,
				max:0
			},
			disabled:true
		})
	}
})